import axios from 'axios';

const url = "http://localhost:5000/api/users";

export const fetchUsers = () => axios.get(url);

export const fetchUserDetails = (id) => axios.get(`${url}/${id}`);

export const createUser = (newUser) => axios.post(url, newUser);

export const updateUser = (id, updateUser) => axios.patch(`${url}/${id}`, updateUser);

export const deleteUser = (id) => axios.delete(`${url}/${id}`);
