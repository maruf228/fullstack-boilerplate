import axios from 'axios';

const url = "http://localhost:5000/api";

// get user info according to token
export const fetchUser = (config) => axios.get(`${url}/auth/user`, config);

// create a user
export const createUser = (user) => axios.post(`${url}/users`, user);

// login to get token
export const loginUser = (user) => axios.post(`${url}/auth`, user);
