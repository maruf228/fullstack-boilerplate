import axios from 'axios';

const url = "http://localhost:5000/api/roles";

export const fetchRoles = () => axios.get(url);

export const fetchRoleDetails = (id) => axios.get(`${url}/${id}`);

export const createRole = (newRole) => axios.post(url, newRole);

export const updateRole = (id, updatedRole) => axios.patch(`${url}/${id}`, updatedRole);

export const deleteRole = (id) => axios.delete(`${url}/${id}`);
