import axios from 'axios';

const url = "http://localhost:5000/api/pages";

export const fetchPages = () => axios.get(url);

export const fetchPageDetails = (id) => axios.get(`${url}/${id}`);

export const createPage = (newPage) => axios.post(url, newPage);

export const updatePage = (id, updatedPage) => axios.patch(`${url}/${id}`, updatedPage);

export const deletePage = (id) => axios.delete(`${url}/${id}`);
