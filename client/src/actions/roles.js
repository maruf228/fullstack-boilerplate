import * as api from '../api/roles';

export const getRoles = () => async (dispatch) => {
  try {
    const { data } = await api.fetchRoles();
    dispatch({ type: 'FETCH_ROLES', payload: data });
  } catch (error) {
    console.log(error.message);
  }
}

export const getRoleDetails = (id) => async (dispatch) => {
  try {
    const { data } = await api.fetchRoleDetails(id);
    return data;
  } catch (error) {
    console.log(error.message);
  }
}

export const createRole = (role) => async(dispatch) => {
  try {
    const { data } = await api.createRole(role);
    dispatch({ type: 'CREATE_ROLE', payload: data });
  } catch (error) {
    console.log(error.message);
  }
}

export const updateRole = (id, role) => async(dispatch) => {
  try {
    const { data } = await api.updateRole(id, role);
    dispatch({ type: 'UPDATE_ROLE', payload: data });
  } catch (error) {
    console.log(error);
  }
}

export const deleteRole = (id) => async(dispatch) => {
  try {
    await api.deleteRole(id);
    dispatch({ type: 'DELETE_ROLE', payload: id });
  } catch (error) {
    console.log(error);
  }
}
