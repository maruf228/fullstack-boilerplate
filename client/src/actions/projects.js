import * as api from '../api/projects';

export const getProjects = () => async (dispatch) => {
  try {
    const { data } = await api.fetchProjects();
    dispatch({ type: 'FETCH_PROJECTS', payload: data });
  } catch (error) {
    console.log(error.message);
  }
}

export const getProjectDetails = (id) => async (dispatch) => {
  try {
    const { data } = await api.fetchProjectDetails(id);
    return data;
  } catch (error) {
    console.log(error.message);
  }
}

export const createProject = (project) => async(dispatch) => {
  try {
    const { data } = await api.createProject(project);
    dispatch({ type: 'CREATE_PROJECT', payload: data });
  } catch (error) {
    console.log(error.message);
  }
}

export const updateProject = (id, project) => async(dispatch) => {
  try {
    const { data } = await api.updateProject(id, project);
    dispatch({ type: 'UPDATE_PROJECT', payload: data });
  } catch (error) {
    console.log(error);
  }
}

export const deleteProject = (id) => async(dispatch) => {
  try {
    await api.deleteProject(id);
    dispatch({ type: 'DELETE_PROJECT', payload: id });
  } catch (error) {
    console.log(error);
  }
}
