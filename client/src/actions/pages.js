import * as api from '../api/pages';

export const getPages = () => async (dispatch) => {
  try {
    const { data } = await api.fetchPages();
    dispatch({ type: 'FETCH_ALL', payload: data });
  } catch (error) {
    console.log(error.message);
  }
}

export const getPageDetails = (id) => async (dispatch) => {
  try {
    const { data } = await api.fetchPageDetails(id);
    return data;
  } catch (error) {
    console.log(error.message);
  }
}

export const createPage = (page) => async(dispatch) => {
  try {
    const { data } = await api.createPage(page);
    dispatch({ type: 'CREATE', payload: data });
  } catch (error) {
    console.log(error.message);
  }
}

export const updatePage = (id, page) => async(dispatch) => {
  try {
    const { data } = await api.updatePage(id, page);
    dispatch({ type: 'UPDATE', payload: data });
  } catch (error) {
    console.log(error);
  }
}

export const deletePage = (id) => async(dispatch) => {
  try {
    await api.deletePage(id);
    dispatch({ type: 'DELETE', payload: id });
  } catch (error) {
    console.log(error);
  }
}
