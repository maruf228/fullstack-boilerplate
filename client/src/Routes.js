import React from 'react';
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';
import Home from './components/Home';
import Admin from './components/admin/dashboard';
import Pages from './components/admin/page/Pages';
import PageForm from './components/admin/page/Form';
import DetailsPage from './components/admin/page/Details';
import Roles from './components/admin/role/Roles';
import RoleForm from './components/admin/role/Form';
import DetailsRole from './components/admin/role/Details';
import Users from './components/admin/user/Users';
import UserForm from './components/admin/user/Form';
import DetailsUser from './components/admin/user/Details';
import Signup from './components/authentication/register';
import Signin from './components/authentication/login';
import Projects from './components/admin/project/Projects';
import ProjectForm from './components/admin/project/Form';
import DetailsProject from './components/admin/project/Details';
import ProtectedRoute from './components/admin/protectedRoute';

const Routes= () => {
  return (
    <Router>
      <Switch>
        <Route path="/" exact component={Home} />
        <Route path="/signin" exact component={Signin} />
        <Route path="/signup" exact component={Signup} />
        <Route path="/admin" exact component={Admin} />

        {/* <ProtectedRoute path="/admin/pages" exact component={Pages} /> */}
        <Route path="/admin/pages" exact component={Pages} />
        <Route path="/admin/pages/create-page" exact component={PageForm} />
        <Route path="/admin/pages/edit/:id" exact component={PageForm} />
        <Route path="/admin/pages/:id" exact component={DetailsPage} />

        <Route path="/admin/roles" exact component={Roles} />
        <Route path="/admin/roles/create-role" exact component={RoleForm} />
        <Route path="/admin/roles/edit/:id" exact component={RoleForm} />
        <Route path="/admin/roles/:id" exact component={DetailsRole} />

        <Route path="/admin/users" exact component={Users} />
        <Route path="/admin/users/edit/:id" exact component={UserForm} />
        <Route path="/admin/users/:id" exact component={DetailsUser} />

        <ProtectedRoute path="/admin/projects" exact component={Projects} />
        <ProtectedRoute path="/admin/projects/create-project" exact component={ProjectForm} />

        {/* <Route path="/admin/projects" exact component={Projects} /> */}
        {/* <Route path="/admin/projects/create-project" exact component={ProjectForm} /> */}
        <Route path="/admin/projects/edit/:id" exact component={ProjectForm} />
        <Route path="/admin/projects/:id" exact component={DetailsProject} />
      </Switch>
    </Router>
  )
}

export default Routes;
