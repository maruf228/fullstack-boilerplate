import React, { useState, useEffect } from 'react';
import { useHistory } from 'react-router-dom';
import { Link } from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux';
import { getUsers, deleteUser } from '../../../actions/users';
import { Table } from 'reactstrap';
import PaginatedCollection from '../../paginatedCollection/paginatedCollection';
import styles from './userlist.module.css';

const PageList = () => {
  const users = useSelector((state) => state.users);
  const [usersToShow, setUsersToShow] = useState();
  const [perPage, setPerPage] = useState(5);
  const [totalPage, setTotalPage] = useState();
  const history = useHistory();
  const dispatch = useDispatch();

  const FetchUsers = async () => {
    await dispatch(getUsers());
  }

  const deleteItem = (id) => {
    dispatch(deleteUser(id));
  }

  const updateItem = (data) => {
    history.push(`/admin/users/edit/${data._id}`, { ...data, is_edit: true });
  }

  const handlePageClick = ({ selected }) => {
    let start = (selected + 1 - 1) * perPage;
    let end = (selected + 1) * perPage;
    setUsersToShow(users.slice(start, end));
  }

  const updatePerPage = (e) => {
    setPerPage(e.target.value);
  }

  useEffect(() => {
    FetchUsers();
  }, []);

  useEffect(() => {
    handlePageClick({selected: 0});
    setTotalPage(users.length / perPage);
  }, [users, perPage]);

  return (
    <>
      <Link
        to="/signup"
        className="btn btn-primary mb-4"
      >
        Add User
      </Link>

      <Table responsive hover className={styles.table}>
        <thead>
          <tr>
            <th className={styles.titleColumn}>Name</th>
            <th>Email</th>
            <th>Role</th>
            <th>Actions</th>
          </tr>
        </thead>

        <tbody>
          {usersToShow?.map((user) => (
            <tr key={user._id}>
              <td>
                <Link to={`/admin/users/${user._id}`}>
                  {user.name}
                </Link>
              </td>

              <td>{user.email}</td>

              <td>{user.role?.title}</td>

              <td>
                <button
                  className={styles.updateBtn}
                  onClick={(e) => updateItem(user)}
                >
                  Edit
                </button>

                <button
                  className={styles.removeBtn}
                  onClick={(e) => deleteItem(user._id, e)}
                >
                  Delete
                </button>
              </td>
            </tr>
          ))}
        </tbody>
      </Table>

      <PaginatedCollection totalPage={totalPage} handlePageClick={handlePageClick} updatePerPage={updatePerPage} />
    </>
  )
}

export default PageList;
