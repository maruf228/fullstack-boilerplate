import Base from '../../base/Base';
import UserList from './UserList';

function App() {
  return (
    <Base>
      <UserList />
    </Base>
  );
}

export default App;
