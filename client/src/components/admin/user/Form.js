import React, { useState, useEffect } from 'react';
import { useHistory, useLocation } from 'react-router-dom';
import Base from '../../base/Base';
import { useDispatch, useSelector } from 'react-redux';
import { updateUser } from '../../../actions/users';
import { getRoles } from '../../../actions/roles';
import { Formik } from 'formik';
import MyInput from '../../form/input/MyInput';
import MySelect from '../../form/select/MySelect';
// import { userSchema } from '../../../validations/user';
import styles from './form.module.css';

function Form() {
  const dispatch = useDispatch();
  const history = useHistory();
  const location = useLocation();
  const roles = useSelector((state) => state.roles);
  const [data, setData] = useState({ title: '' });
  const [isEdit, setIsEdit] = useState(false);
  const [id, setId] = useState();

  const setValues = (values) => {
    setIsEdit(values.is_edit);
    setId(values._id);
    setData({
      name: values.name,
      role: values.roleId
    })
  }

  const FetchRoles = async () => {
    await dispatch(getRoles());
  }

  useEffect(() => {
    if(location.state) {
      setValues(location.state);
    }
    FetchRoles();
  }, [location.state]);

  return (
    <Base>
      <div className={styles.formContainer}>
        <Formik
          initialValues={data}

          enableReinitialize={true}

          // validationSchema={userSchema}

          onSubmit={(values, { setSubmitting }) => {
            if (isEdit) {
              dispatch(updateUser(id,values));
            }

            setTimeout(() => {
              history.push('/admin/users');
              setSubmitting(false);
            }, 400);

          }}
        >
          {({
            values,
            errors,
            touched,
            handleChange,
            handleBlur,
            handleSubmit,
            isSubmitting,
          }) => (
            <form onSubmit={handleSubmit}>
              <MyInput
                label="Name"
                name="name"
                type="text"
                placeholder="name"
              />

              <MySelect
                label="Role"
                name="role"
                data={roles}
              />

              <button type="submit" disabled={isSubmitting} className={styles.submitButton}>
                {isEdit ? "Update" : "Create"}
              </button>
            </form>
          )}
        </Formik>
      </div>
    </Base>
  );
}

export default Form;

