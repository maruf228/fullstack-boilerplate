import React, { useState, useEffect } from 'react';
import Base from '../../base/Base';
import { getUserDetails } from '../../../actions/users';
import { useDispatch } from 'react-redux';
import styles from './details.module.css';

const Details = (props) => {
  const [user, setUser] = useState();
  const dispatch = useDispatch();

  const userDetails = async () => {
    let data = await dispatch(getUserDetails(props.match.params.id));
    setUser(data);
  }
  useEffect(() => {
    userDetails();
  }, []);

  return (
    <Base>
      <div className={styles.wrapper}>
        {user &&
          <div>
            <div className={styles.perItem}>
              <div className={styles.key}>Name:</div>

              <div className={styles.value}>{user.name}</div>
            </div>

            <div className={styles.perItem}>
              <div className={styles.key}>Email:</div>

              <div className={styles.value}>{user.email}</div>
            </div>

            <div className={styles.perItem}>
              <div className={styles.key}>Role:</div>

              <div className={styles.value}>{user.role}</div>
            </div>
          </div>
        }
      </div>
    </Base>
  )
}

export default Details;
