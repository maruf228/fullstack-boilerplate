import React, { useState, useEffect } from 'react';
import { useHistory, useLocation } from 'react-router-dom';
import Base from '../../base/Base';
import { useDispatch } from 'react-redux';
import { createPage, updatePage } from '../../../actions/pages';
import { Formik } from 'formik';
import MyInput from '../../form/input/MyInput';
import { pageSchema } from '../../../validations/page';
import styles from './form.module.css';

function Form() {
  const dispatch = useDispatch();
  const history = useHistory();
  const location = useLocation();
  const [data, setData] = useState({
    title: '',
    excerpt: '',
    body: ''
  });
  const [isEdit, setIsEdit] = useState(false);
  const [id, setId] = useState();

  const setValues = (values) => {
    setIsEdit(values.is_edit);
    setId(values._id);
    setData({
      title: values.title,
      excerpt: values.excerpt,
      body: values.body
    })
  }

  useEffect(() => {
    if(location.state) {
      setValues(location.state);
    }
  }, [location.state]);

  return (
    <Base>
      <div className={styles.formContainer}>
        <Formik
          initialValues={data}

          enableReinitialize={true}

          validationSchema={pageSchema}

          onSubmit={(values, { setSubmitting }) => {
            if (isEdit) {
              dispatch(updatePage(id,values));
            } else {
              dispatch(createPage(values));
            }

            setTimeout(() => {
              history.push('/admin/pages');
              setSubmitting(false);
            }, 400);

          }}
        >
          {({
            values,
            errors,
            touched,
            handleChange,
            handleBlur,
            handleSubmit,
            isSubmitting,
          }) => (
            <form onSubmit={handleSubmit}>
              <MyInput
                label="Title"
                name="title"
                type="text"
                placeholder="title"
              />

              <MyInput
                label="Excerpt"
                name="excerpt"
                type="text"
                placeholder="excerpt"
              />

              <MyInput
                label="Body"
                name="body"
                type="text"
                placeholder="body"
              />

              <button type="submit" disabled={isSubmitting} className={styles.submitButton}>
                {isEdit ? "Update" : "Create"}
              </button>
            </form>
          )}
        </Formik>
      </div>
    </Base>
  );
}

export default Form;

