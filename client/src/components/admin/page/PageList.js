import React, { useState, useEffect } from 'react';
import { useHistory } from 'react-router-dom';
import { Link } from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux';
import { getPages, deletePage } from '../../../actions/pages';
import { Table } from 'reactstrap';
import PaginatedCollection from '../../paginatedCollection/paginatedCollection';
import styles from './pagelist.module.css';

const PageList = () => {
  const pages = useSelector((state) => state.pages);
  const [pagesToShow, setPagesToShow] = useState();
  const [perPage, setPerPage] = useState(5);
  const [totalPage, setTotalPage] = useState();
  const history = useHistory();
  const dispatch = useDispatch();

  const FetchPages = async () => {
    await dispatch(getPages());
  }

  const deleteItem = (id) => {
    dispatch(deletePage(id));
  }

  const updateItem = (data) => {
    history.push(`/admin/pages/edit/${data._id}`, { ...data, is_edit: true });
  }

  const handlePageClick = ({ selected }) => {
    let start = (selected + 1 - 1) * perPage;
    let end = (selected + 1) * perPage;
    setPagesToShow(pages.slice(start, end));
  }

  const updatePerPage = (e) => {
    setPerPage(e.target.value);
  }

  useEffect(() => {
    FetchPages();
  }, []);

  useEffect(() => {
    handlePageClick({selected: 0});
    setTotalPage(pages.length / perPage);
  }, [pages, perPage]);

  return (
    <>
      <Link
        to="/admin/pages/create-page"
        className="btn btn-primary mb-4"
      >
        Add Page
      </Link>

      <Table responsive hover className={styles.table}>
        <thead>
          <tr>
            <th className={styles.titleColumn}>Title</th>
            <th>Excerpt</th>
            <th>Actions</th>
          </tr>
        </thead>

        <tbody>
          {pagesToShow?.map((page) => (
            <tr key={page._id}>
              <td>
                <Link to={`/admin/pages/${page._id}`}>
                  {page.title}
                </Link>
              </td>

              <td>{page.excerpt}</td>
              <td>
                <button
                  className={styles.updateBtn}
                  onClick={(e) => updateItem(page)}
                >
                  Edit
                </button>

                <button
                  className={styles.removeBtn}
                  onClick={(e) => deleteItem(page._id, e)}
                >
                  Delete
                </button>
              </td>
            </tr>
          ))}
        </tbody>
      </Table>

      <PaginatedCollection totalPage={totalPage} handlePageClick={handlePageClick} updatePerPage={updatePerPage} />
    </>
  )
}

export default PageList;
