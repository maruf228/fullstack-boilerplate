import Base from '../../base/Base';
import PageList from './PageList';

function App() {
  return (
    <Base>
      <PageList />
    </Base>
  );
}

export default App;
