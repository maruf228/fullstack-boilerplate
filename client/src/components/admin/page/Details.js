import React, { useState, useEffect } from 'react';
import Base from '../../base/Base';
import { getPageDetails } from '../../../actions/pages';
import { useDispatch } from 'react-redux';
import styles from './Details.module.css';

const Details = (props) => {
  const [page, setPage] = useState();
  const dispatch = useDispatch();

  const pageDetails = async () => {
    let data = await dispatch(getPageDetails(props.match.params.id));
    setPage(data);
    console.log(data);
  }
  useEffect(() => {
    pageDetails();
  }, []);

  return (
    <Base>
      <div className={styles.wrapper}>
        {page &&
          <div>
            <div className={styles.perItem}>
              <div className={styles.key}>Title:</div>

              <div className={styles.value}>{page.title}</div>
            </div>

            <div className={styles.perItem}>
              <div className={styles.key}>Excerpt:</div>

              <div className={styles.value}>{page.excerpt}</div>
            </div>

            <div className={styles.perItem}>
              <div className={styles.key}>Body:</div>

              <div className={styles.value}>{page.body}</div>
            </div>

            <div className={styles.perItem}>
              <div className={styles.key}>Image:</div>

              <div className={styles.value}>
                <img src={`http://localhost:5000/uploads/images/${page.image}`} alt="image" className={styles.profileImage}/>
              </div>
            </div>
          </div>

        }
      </div>
    </Base>
  )
}

export default Details;
