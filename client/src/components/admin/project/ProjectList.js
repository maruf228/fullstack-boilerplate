import React, { useState, useEffect } from 'react';
import { useHistory } from 'react-router-dom';
import { Link } from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux';
import { getProjects, deleteProject } from '../../../actions/projects';
import { Table } from 'reactstrap';
import PaginatedCollection from '../../paginatedCollection/paginatedCollection';
import styles from './projectlist.module.css';

const ProjectList = () => {
  const projects = useSelector((state) => state.projects);
  const [projectsToShow, setProjectsToShow] = useState();

  const [perPage, setPerPage] = useState(5);
  const [totalPage, setTotalPage] = useState();

  const history = useHistory();
  const dispatch = useDispatch();

  const FetchProjects = async () => {
    await dispatch(getProjects());
  }

  const deleteItem = (id) => {
    dispatch(deleteProject(id));
  }

  const updateItem = (data) => {
    history.push(`/admin/projects/edit/${data._id}`, { ...data, is_edit: true });
  }

  const handlePageClick = ({ selected }) => {
    let start = (selected + 1 - 1) * perPage;
    let end = (selected + 1) * perPage;
    setProjectsToShow(projects.slice(start, end));
  }

  const updatePerPage = (e) => {
    setPerPage(e.target.value);
  }

  useEffect(() => {
    FetchProjects();
  }, []);

  useEffect(() => {
    handlePageClick({selected: 0});
    setTotalPage(projects.length / perPage);
  }, [projects, perPage]);

  return (
    <>
      <Link
        to="/admin/projects/create-project"
        className="btn btn-primary mb-4"
      >
        Add Project
      </Link>

      <Table responsive hover className={styles.table}>
        <thead>
          <tr>
            <th className={styles.titleColumn}>Title</th>
            <th>Excerpt</th>
            <th>Actions</th>
          </tr>
        </thead>

        <tbody>
          {projectsToShow?.map((project) => (
            <tr key={project._id}>
              <td>
                <Link to={`/admin/projects/${project._id}`}>
                  {project.title}
                </Link>
              </td>

              <td className={styles.excerpt}>{project.excerpt}</td>
              <td>
                <button
                  className={styles.updateBtn}
                  onClick={(e) => updateItem(project)}
                >
                  Edit
                </button>

                <button
                  className={styles.removeBtn}
                  onClick={(e) => deleteItem(project._id, e)}
                >
                  Delete
                </button>
              </td>
            </tr>
          ))}
        </tbody>
      </Table>

      <PaginatedCollection totalPage={totalPage} handlePageClick={handlePageClick} updatePerPage={updatePerPage} />
    </>
  )
}

export default ProjectList;
