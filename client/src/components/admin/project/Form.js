import React, { useState, useEffect } from 'react';
import { useHistory, useLocation } from 'react-router-dom';
import Base from '../../base/Base';
import { useDispatch } from 'react-redux';
import { createProject, updateProject } from '../../../actions/projects';
import { Formik } from 'formik';
import MyInput from '../../form/input/MyInput';
import { projectSchema } from '../../../validations/project';
import styles from './form.module.css';

function Form() {
  const dispatch = useDispatch();
  const history = useHistory();
  const location = useLocation();

  const [title, setTitle] = useState();
  const [excerpt, setExcerpt] = useState();
  const [siteLink, setSiteLink] = useState();
  const [position, setPosition] = useState();
  const [file, setFile] = useState();

  const [isEdit, setIsEdit] = useState(false);
  const [id, setId] = useState();

  const [data, setData] = useState({
    title: '',
    excerpt: '',
    siteLink: '',
    position: ''
  });

  const setValues = (values) => {
    setIsEdit(true);
    setId(values._id);
    setData({
      title: values.title,
      excerpt: values.excerpt,
      siteLink: values.siteLink,
      position: values.position
    });

    setTitle(values.title);
    setExcerpt(values.excerpt);
    setSiteLink(values.siteLink);
    setPosition(values.position);
  }

  const handleChange = (name, value) => {
    setData((prevState) => ({
      ...prevState,
      [name]: value,
    }));
  }

  const handleSubmit = async(e) => {
    e.preventDefault();
    if(isEdit) {
      dispatch(updateProject(id, data));

      setTimeout(() => {
        history.push('/admin/projects');
      }, 400);
    } else {
      const formData = new FormData();
      formData.append('title', data.title);
      formData.append('excerpt', data.excerpt);
      formData.append('siteLink', data.siteLink);
      formData.append('position', data.position);
      formData.append('image', file);
      dispatch(createProject(formData));

      // dispatch(createProject({...data, image: file}));

      setTimeout(() => {
        history.push('/admin/projects');
      }, 400);
    }
  }

  useEffect(() => {
    if(location.state) {
      setValues(location.state);
    }
  }, [location.state]);

  return (
    <Base>
      <div className={styles.formContainer}>
        <form onSubmit={handleSubmit}>
          <div>
            <div>Title</div>
            <input
              id="title"
              name="title"
              type='text'
              value={data.title}
              onChange={(e) => {handleChange('title', e.target.value)}}
            />
          </div>

          <div>
            <div>excerpt</div>
            <input
              id="excerpt"
              name="excerpt"
              type='text'
              value={data.excerpt}
              onChange={(e) => {handleChange('excerpt', e.target.value)}}
            />
          </div>

          <div>
            <div>Site Link</div>
            <input
              id="siteLink"
              name="siteLink"
              type='text'
              value={data.siteLink}
              onChange={(e) => {handleChange('siteLink', e.target.value)}}
            />
          </div>

          <div>
            <div>position</div>
            <input
              id="position"
              name="position"
              type='number'
              value={data.position}
              onChange={(e) => {handleChange('position', parseInt(e.target.value))}}
            />
          </div>

          <div>
            <div>image</div>
            <input
              id="file"
              name="image"
              type='file'
              onChange={(e) => { setFile(e.currentTarget.files[0]) }}
            />
          </div>

          <button type="submit" className={styles.submitButton}>
            {isEdit ? "Update" : "Create"}
          </button>
        </form>
      </div>
    </Base>
  );
}

export default Form;

