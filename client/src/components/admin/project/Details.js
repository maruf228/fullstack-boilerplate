import React, { useState, useEffect } from 'react';
import Base from '../../base/Base';
import { getProjectDetails } from '../../../actions/projects';
import { useDispatch } from 'react-redux';
import styles from './Details.module.css';

const Details = (props) => {
  const [project, setProject] = useState();
  const dispatch = useDispatch();

  const projectDetails = async () => {
    let data = await dispatch(getProjectDetails(props.match.params.id));
    setProject(data);
  }
  useEffect(() => {
    projectDetails();
  }, []);

  return (
    <Base>
      <div className={styles.wrapper}>
        {project &&
          <div>
            <div className={styles.perItem}>
              <div className={styles.key}>Title:</div>

              <div className={styles.value}>{project.title}</div>
            </div>

            <div className={styles.perItem}>
              <div className={styles.key}>Excerpt:</div>

              <div className={styles.value}>{project.excerpt}</div>
            </div>

            <div className={styles.perItem}>
              <div className={styles.key}>Site Link:</div>

              <div className={styles.value}><a href={project.siteLink} target='_blank'>{project.siteLink}</a></div>
            </div>

            <div className={styles.perItem}>
              <div className={styles.key}>Position:</div>

              <div className={styles.value}>{project.position}</div>
            </div>

            <div className={styles.perItem}>
              <div className={styles.key}>Image:</div>

              <div className={styles.value}>
                <img src={`http://localhost:5000/uploads/images/${project.image}`} alt="image" className={styles.profileImage}/>
              </div>
            </div>
          </div>

        }
      </div>
    </Base>
  )
}

export default Details;
