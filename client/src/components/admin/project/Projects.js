import Base from '../../base/Base';
import ProjectList from './ProjectList';

function App() {
  return (
    <Base>
      <ProjectList />
    </Base>
  );
}

export default App;
