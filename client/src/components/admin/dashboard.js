import React from 'react';
import { logout } from '../../actions/authActions';
import Base from '../base/Base';
import { useDispatch } from 'react-redux';
import styles from './dashboard.module.css';

function App() {
  const dispatch = useDispatch();

  const logoutUser = () => {
    dispatch(logout());
  }

  return (
    <Base>
      <h2>Admin Dashboard</h2>

      <button className={styles.logoutBtn} onClick={() => logoutUser()}>
        Logout
      </button>
    </Base>
  );
}

export default App;
