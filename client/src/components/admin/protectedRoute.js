import React, { useState, useEffect } from "react";
import { useSelector } from 'react-redux';
import { Route, Redirect } from "react-router-dom";

const ProtectedRoute = ({ component: Component, path }) => {
  const auth = useSelector((state) => state.auth);

  return (
    <>
      {(auth.isAuthenticated) ?
        <Component />
        :
        <Redirect
          to={{
            pathname: "/signin",
            state: { destinationRoute: path }
          }}
        />

      }
    </>
  );
};

export default ProtectedRoute;
