import React, { useState, useEffect } from 'react';
import { useHistory } from 'react-router-dom';
import { Link } from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux';
import { getRoles, deleteRole } from '../../../actions/roles';
import { Table } from 'reactstrap';
import PaginatedCollection from '../../paginatedCollection/paginatedCollection';
import styles from './rolelist.module.css';

const PageList = () => {
  const roles = useSelector((state) => state.roles);
  const [rolesToShow, setRolesToShow] = useState();
  const [perPage, setPerPage] = useState(5);
  const [totalPage, setTotalPage] = useState();
  const history = useHistory();
  const dispatch = useDispatch();

  const FetchRoles = async () => {
    await dispatch(getRoles());
  }

  const deleteItem = (id) => {
    dispatch(deleteRole(id));
  }

  const updateItem = (data) => {
    history.push(`/admin/roles/edit/${data._id}`, { ...data, is_edit: true });
  }

  const handlePageClick = ({ selected }) => {
    let start = (selected + 1 - 1) * perPage;
    let end = (selected + 1) * perPage;
    setRolesToShow(roles.slice(start, end));
  }

  const updatePerPage = (e) => {
    setPerPage(e.target.value);
  }

  useEffect(() => {
    FetchRoles();
  }, []);

  useEffect(() => {
    handlePageClick({selected: 0});
    setTotalPage(roles.length / perPage);
  }, [roles, perPage]);

  return (
    <>
      <Link
        to="/admin/roles/create-role"
        className="btn btn-primary mb-4"
      >
        Add Role
      </Link>

      <Table responsive hover className={styles.table}>
        <thead>
          <tr>
            <th className={styles.titleColumn}>Title</th>
            <th>Actions</th>
          </tr>
        </thead>

        <tbody>
          {rolesToShow?.map((role) => (
            <tr key={role._id}>
              <td>
                <Link to={`/admin/roles/${role._id}`}>
                  {role.title}
                </Link>
              </td>

              <td>
                <button
                  className={styles.updateBtn}
                  onClick={(e) => updateItem(role)}
                >
                  Edit
                </button>

                <button
                  className={styles.removeBtn}
                  onClick={(e) => deleteItem(role._id, e)}
                >
                  Delete
                </button>
              </td>
            </tr>
          ))}
        </tbody>
      </Table>

      <PaginatedCollection totalPage={totalPage} handlePageClick={handlePageClick} updatePerPage={updatePerPage} />
    </>
  )
}

export default PageList;
