import React, { useState, useEffect } from 'react';
import { useHistory, useLocation } from 'react-router-dom';
import Base from '../../base/Base';
import { useDispatch } from 'react-redux';
import { createRole, updateRole } from '../../../actions/roles';
import { Formik } from 'formik';
import MyInput from '../../form/input/MyInput';
import { roleSchema } from '../../../validations/role';
import styles from './form.module.css';

function Form() {
  const dispatch = useDispatch();
  const history = useHistory();
  const location = useLocation();
  const [data, setData] = useState({ title: '' });
  const [isEdit, setIsEdit] = useState(false);
  const [id, setId] = useState();

  const setValues = (values) => {
    setIsEdit(values.is_edit);
    setId(values._id);
    setData({
      title: values.title
    })
  }

  useEffect(() => {
    if(location.state) {
      setValues(location.state);
    }
  }, [location.state]);

  return (
    <Base>
      <div className={styles.formContainer}>
        <Formik
          initialValues={data}

          enableReinitialize={true}

          validationSchema={roleSchema}

          onSubmit={(values, { setSubmitting }) => {
            if (isEdit) {
              dispatch(updateRole(id,values));
            } else {
              dispatch(createRole(values));
            }

            setTimeout(() => {
              history.push('/admin/roles');
              setSubmitting(false);
            }, 400);

          }}
        >
          {({
            values,
            errors,
            touched,
            handleChange,
            handleBlur,
            handleSubmit,
            isSubmitting,
          }) => (
            <form onSubmit={handleSubmit}>
              <MyInput
                label="Title"
                name="title"
                type="text"
                placeholder="title"
              />

              <button type="submit" disabled={isSubmitting} className={styles.submitButton}>
                {isEdit ? "Update" : "Create"}
              </button>
            </form>
          )}
        </Formik>
      </div>
    </Base>
  );
}

export default Form;

