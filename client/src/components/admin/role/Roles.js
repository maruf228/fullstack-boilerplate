import Base from '../../base/Base';
import RoleList from './RoleList';

function App() {
  return (
    <Base>
      <RoleList />
    </Base>
  );
}

export default App;
