import React, { useState, useEffect } from 'react';
import Base from '../../base/Base';
import { getRoleDetails } from '../../../actions/roles';
import { useDispatch } from 'react-redux';
import styles from './details.module.css';

const Details = (props) => {
  const [role, setRole] = useState();
  const dispatch = useDispatch();

  const roleDetails = async () => {
    let data = await dispatch(getRoleDetails(props.match.params.id));
    setRole(data);
  }
  useEffect(() => {
    roleDetails();
  }, []);

  return (
    <Base>
      <div className={styles.wrapper}>
        {role &&
          <div>
            <div className={styles.perItem}>
              <div className={styles.key}>Title:</div>

              <div className={styles.value}>{role.title}</div>
            </div>

            <div className={styles.perItem}>
              <div className={styles.key}>Created At:</div>

              <div className={styles.value}>{role.createdAt}</div>
            </div>
          </div>

        }
      </div>
    </Base>
  )
}

export default Details;
