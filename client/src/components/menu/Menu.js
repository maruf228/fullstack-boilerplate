import React from 'react'
import { Link, withRouter } from 'react-router-dom';
import { useSelector } from 'react-redux';
import './Menu.css';

const currentTab = (history, path) => {
  if (history.location.pathname === path) {
    return {color: "#fa004a"}
  } else {
    return {color: "#ffffff"}
  }
}

const Menu = ({ history, path }) => {
  const auth = useSelector((state) => state.auth);

  return (
    <div className="nav-wrapper">
      <div className="nav-item">
        <Link to="/" style={currentTab(history, "/")} className="nav-link">
          Home
        </Link>
      </div>

      <div className="nav-item">
        <Link to="/signup" style={currentTab(history, "/signup")} className="nav-link">
          Signup
        </Link>
      </div>

      {!auth.isAuthenticated &&
        <div className="nav-item">
          <Link to="/signin" style={currentTab(history, "/signin")} className="nav-link">
            Signin
          </Link>
        </div>
      }

      {auth.isAuthenticated &&
        <div className="nav-item">
          <Link to="/admin" style={currentTab(history, "/admin")} className="nav-link">
            Admin
          </Link>
        </div>
      }

      {auth.isAuthenticated &&
        <div className="nav-item">
          <Link to="/admin/users" style={currentTab(history, "/admin/users")} className="nav-link">
            Users
          </Link>
        </div>
      }

      {auth.isAuthenticated &&
        <div className="nav-item">
          <Link to="/admin/pages" style={currentTab(history, "/admin/pages")} className="nav-link">
            Pages
          </Link>
        </div>
      }

      {auth.isAuthenticated &&
        <div className="nav-item">
          <Link to="/admin/projects" style={currentTab(history, "/admin/projects")} className="nav-link">
            Projects
          </Link>
        </div>
      }
    </div>
  )
}

export default withRouter(Menu);
