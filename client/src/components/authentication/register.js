import React, { useState, useEffect } from 'react';
import Base from '../base/Base';
import { useDispatch } from 'react-redux';
import { Formik } from 'formik';
import MyInput from '../form/input/MyInput';
import { userSchema } from '../../validations/user';
import { createUser } from '../../actions/authActions';
import styles from './register.module.css';

function Form() {
  const dispatch = useDispatch();
  const [data, setData] = useState({
    name: '',
    email: '',
    password: ''
  });

  return (
    <Base>
      <div className={styles.formContainer}>
        <div className={styles.heading}>Registration Form</div>

        <Formik
          initialValues={data}

          validationSchema={userSchema}

          onSubmit={(values, { setSubmitting }) => {
            dispatch(createUser(values));

            setTimeout(() => {
              setSubmitting(false);
            }, 400);
          }}
        >
          {({
            values,
            errors,
            touched,
            handleChange,
            handleBlur,
            handleSubmit,
            isSubmitting,
          }) => (
            <form onSubmit={handleSubmit}>
              <MyInput
                // label="Name"
                name="name"
                type="text"
                placeholder="name"
                className={styles.input}
              />

              <MyInput
                // label="Email"
                name="email"
                type="email"
                placeholder="email"
                className={styles.input}
              />

              <MyInput
                // label="Password"
                name="password"
                type="password"
                placeholder="password"
                className={styles.input}
              />

              <button type="submit" disabled={isSubmitting} className={styles.submitButton}>
                Register
              </button>
            </form>
          )}
        </Formik>
      </div>
    </Base>
  );
}

export default Form;

