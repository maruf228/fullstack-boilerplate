import React, { useState, useEffect } from 'react';
import Base from '../base/Base';
import { useDispatch, useSelector } from 'react-redux';
import { Formik } from 'formik';
import MyInput from '../form/input/MyInput';
import { loginSchema } from '../../validations/login';
import { login } from '../../actions/authActions';
import styles from './login.module.css';
import { clearErrors } from '../../actions/errors';
import { useHistory, useLocation } from 'react-router-dom';

function Form() {
  const location = useLocation();
  const history = useHistory();
  const error = useSelector((state) => state.error);
  const auth = useSelector((state) => state.auth);

  const dispatch = useDispatch();
  const [data, setData] = useState({
    email: '',
    password: ''
  });

  useEffect(() => {
    dispatch(clearErrors());
  }, [data])

  useEffect(() => {
    if (auth.isAuthenticated) {
      if(location.state != null) {
        history.push(`${location.state.destinationRoute}`)
      }
    }
  }, [auth])

  return (
    <Base>
      <div className={styles.formContainer}>
        <div className={styles.heading}>Login</div>

        <Formik
          initialValues={data}

          validationSchema={loginSchema}

          onSubmit={(values, { setSubmitting }) => {
            dispatch(login(values));

            setTimeout(() => {
              setSubmitting(false);
            }, 400);
          }}
        >
          {({
            values,
            errors,
            touched,
            handleChange,
            handleBlur,
            handleSubmit,
            isSubmitting,
          }) => (
            <form onSubmit={handleSubmit}>
              <MyInput
                // label="Email"
                name="email"
                type="email"
                placeholder="email"
                className={styles.input}
              />

              <MyInput
                // label="Password"
                name="password"
                type="password"
                placeholder="password"
                className={styles.input}
              />

              { error.msg.message &&
                <div className={styles.errorContainer}>
                  {error.msg.message}
                </div>
              }

              <button type="submit" disabled={isSubmitting} className={styles.submitButton}>
                Log in
              </button>
            </form>
          )}
        </Formik>
      </div>
    </Base>
  );
}

export default Form;

