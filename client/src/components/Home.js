import React, { useEffect } from "react";
import Base from "./base/Base";
import { getUser } from '../actions/authActions';
import { useDispatch } from 'react-redux';
import styles from './home.module.css';

export default function Project() {
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(getUser());
  }, []);

  return (
    <Base>
      <div className={styles.text}>Fullstack Boilerplate</div>
    </Base>
  )
}
