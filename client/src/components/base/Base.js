import React from 'react'
import Menu from '../menu/Menu';
import styles from './Base.module.css';

const Base = ({
  children
}) => {
  return (
    <div>
      <div className={styles.baseMenu}>
        <Menu />
      </div>

      <div className={styles.childDiv}>{children}</div>
    </div>
  );
}

export default Base;
