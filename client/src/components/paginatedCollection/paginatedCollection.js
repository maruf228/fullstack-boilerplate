import ReactPaginate from 'react-paginate';
import styles from './paginated.module.css';

export default function PaginatedCollection(props) {
  return (
    <div className={styles.wrapper}>
      <div className={styles.selectionWrapper}>
        <label htmlFor="perPage">Per page:</label>

        <select name="per-page" id="perPage" className={styles.select} onChange={props.updatePerPage}>
          <option value={5}>5</option>
          <option value={10}>10</option>
          <option value={15}>15</option>
          <option value={20}>20</option>
        </select>
      </div>

      <ReactPaginate
        previousLabel={'<'}
        nextLabel={'>'}
        breakLabel={'...'}
        pageCount={props.totalPage}
        marginPagesDisplayed={2}
        pageRangeDisplayed={1}
        onPageChange={props.handlePageClick}
        containerClassName={'pagination'}
        breakClassName={'page-link'}
        pageClassName={'page-item'}
        pageLinkClassName={'page-link'}
        activeClassName={'active'}
        previousClassName={'page-link previous'}
        nextClassName={'page-link next'}
      />
    </div>
  )
}
