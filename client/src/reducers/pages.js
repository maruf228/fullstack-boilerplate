export default (pages = [], action) => {
  switch (action.type) {
    case 'FETCH_ALL':
      return action.payload;
    case 'CREATE':
      return [ ...pages, action.payload];
    case 'UPDATE':
      return pages.map((page) => page._id === action.payload._id ? action.payload : page);
    case 'DELETE':
      return pages.filter((page) => page._id !== action.payload);
    default:
      return pages;
  }
}
