export default (roles = [], action) => {
  switch (action.type) {
    case 'FETCH_ROLES':
      return action.payload;
    case 'CREATE_ROLE':
      return [ ...roles, action.payload];
    case 'UPDATE_ROLE':
      return roles.map((role) => role._id === action.payload._id ? action.payload : role);
    case 'DELETE_ROLE':
      return roles.filter((role) => role._id !== action.payload);
    default:
      return roles;
  }
}

// eslint import/no-anonymous-default-export: [2, {"allowArrowFunction": true}]
