import { combineReducers } from 'redux';
import pages from './pages';
import auth from './auth';
import error from './error';
import roles from './roles';
import users from './users';
import projects from './projects';

export default combineReducers({ pages, error, auth, roles, users, projects });
