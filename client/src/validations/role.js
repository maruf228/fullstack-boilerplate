import * as Yup from "yup";

export const roleSchema = Yup.object().shape({
  title: Yup.string()
    .min(2, 'Too Short!')
    .max(15, 'Must be 15 characters or less')
    .required('Required')
});
