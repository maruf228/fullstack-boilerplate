import * as Yup from "yup";

export const loginSchema = Yup.object().shape({
  email: Yup.string()
    .email('Invalid email')
    .required('Required'),
  password: Yup.string()
    .min(4, 'Too Short!')
    .max(12, 'Must be 12 characters or less')
    .required('Required')
});
