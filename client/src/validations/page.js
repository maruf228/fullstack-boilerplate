import * as Yup from "yup";

export const pageSchema = Yup.object().shape({
  title: Yup.string()
    .min(2, 'Too Short!')
    .max(15, 'Must be 15 characters or less')
    .required('Required'),
  excerpt: Yup.string()
    .min(2, 'Too Short!')
    .max(50, 'Must be 50 characters or less')
    .required('Required'),
  body: Yup.string()
    .min(2, 'Too Short!')
    .max(500, 'Must be 500 characters or less')
    .required('Required'),
  // email: Yup.string().email('Invalid email').required('Required'),
  // acceptedTerms: Yup.boolean().required("Required").oneOf([true], "You must accept the terms and conditions."),
  // jobType: Yup.string().oneOf(["designer", "development", "product", "other"],"Invalid Job Type").required("Required")
});
