import * as Yup from "yup";

export const userSchema = Yup.object().shape({
  name: Yup.string()
    .min(2, 'Too Short!')
    .max(40, 'Must be 40 characters or less')
    .required('Required'),
  email: Yup.string()
    .email('Invalid email')
    .required('Required'),
  password: Yup.string()
    .min(4, 'Too Short!')
    .max(12, 'Must be 12 characters or less')
    .required('Required')
});
