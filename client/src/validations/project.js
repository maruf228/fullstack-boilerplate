import * as Yup from "yup";

export const projectSchema = Yup.object().shape({
  title: Yup.string()
    .min(2, 'Too Short!')
    .max(50, 'Must be 50 characters or less')
    .required('Required'),
  excerpt: Yup.string()
    .min(2, 'Too Short!')
    .max(200, 'Must be 200 characters or less')
    .required('Required'),
  siteLink: Yup.string()
    .min(2, 'Too Short!')
    .max(100, 'Must be 100 characters or less')
    .required('Required'),
  // email: Yup.string().email('Invalid email').required('Required'),
  // acceptedTerms: Yup.boolean().required("Required").oneOf([true], "You must accept the terms and conditions."),
  // jobType: Yup.string().oneOf(["designer", "development", "product", "other"],"Invalid Job Type").required("Required")
});
