import mongoose from 'mongoose';

const pageSchema = mongoose.Schema({
  title: String,
  excerpt: String,
  body: String,
  image: String,
  createdAt: {
      type: Date,
      default: new Date()
  }
})

const Page = mongoose.model('page', pageSchema);

export default Page;
