import mongoose from 'mongoose';

const userSchema = mongoose.Schema({
  name: {
    type: String,
    required: true
  },
  email: {
    type: String,
    required: true,
    unique: true
  },
  password: {
    type: String,
    required: true
  },
  role: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'role'
  },
  registeredAt: {
      type: Date,
      default: new Date()
  }
})

const User = mongoose.model('user', userSchema);

export default User;
