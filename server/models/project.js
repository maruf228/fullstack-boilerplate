import mongoose from 'mongoose';

const projectSchema = mongoose.Schema({
  title: String,
  excerpt: String,
  siteLink: String,
  image: String,
  position: Number,
  createdAt: {
    type: Date,
    default: new Date()
  }
})

const Project = mongoose.model('project', projectSchema);

export default Project;
