import mongoose from 'mongoose';

const roleSchema = mongoose.Schema({
    title: String,
    createdAt: {
        type: Date,
        default: new Date()
    }
})

const Role = mongoose.model('role', roleSchema);

export default Role;
