import express from 'express';
import bodyParser from 'body-parser';
import mongoose from 'mongoose';
import cors from 'cors';
import dotenv from 'dotenv';
import config from 'config';
import pageRoutes from './routes/page.js';
import userRoutes from './routes/user.js';
import authRoutes from './routes/auth.js';
import roleRoutes from './routes/role.js';
import projectRoutes from './routes/project.js';

const app = express();
dotenv.config();




app.use(bodyParser.json({ limit: "30mb", extended: true }));
app.use(bodyParser.urlencoded({ limit: "30mb", extended: true}));
app.use(cors());

app.use('/api/pages', pageRoutes);
app.use('/api/users', userRoutes);
app.use('/api/auth', authRoutes);
app.use('/api/roles', roleRoutes);
app.use('/api/projects', projectRoutes);

app.use(express.static('public'));

const PORT = process.env.PORT || 5000;

mongoose
    .connect(config.get('mongoUrl'), { useNewUrlParser: true, useCreateIndex: true, useUnifiedTopology: true })
    .then(() => app.listen(PORT, () => console.log(`Server running on port: ${PORT}`)))
    .catch((error) => console.log(error.message));

mongoose.set('useFindAndModify', false);
