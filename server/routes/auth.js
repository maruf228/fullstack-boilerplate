import express from 'express';
import { loginUser, getUser } from '../controllers/auth.js';
import { auth } from '../middleware/auth.js';

const router = express.Router();

// for login which return token as well as user if any
router.post('/', loginUser);

// for getting existing user info after sending proper token
router.get('/user', auth, getUser);

export default router;
