import express from 'express';
import { createRole, getRoles, updateRole, deleteRole, getRoleDetails } from '../controllers/role.js';
// import { auth } from '../middleware/auth.js';

const router = express.Router();

router.get('/', getRoles);
router.get('/:id', getRoleDetails);
router.post('/', createRole);
router.patch('/:id', updateRole);
router.delete('/:id', deleteRole);
export default router;
