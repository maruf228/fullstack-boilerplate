import express from 'express';
import { createPage, getPages, updatePage, deletePage, getPageDetails } from '../controllers/page.js';
// import { auth } from '../middleware/auth.js';

const router = express.Router();

router.get('/', getPages);
router.get('/:id', getPageDetails);
router.post('/', createPage);
router.patch('/:id', updatePage);
router.delete('/:id', deletePage);
export default router;
