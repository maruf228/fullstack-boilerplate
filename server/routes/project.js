import express from 'express';
import { createProject, getProjects, updateProject, deleteProject, getProjectDetails } from '../controllers/project.js';
// import { auth } from '../middleware/auth.js';
import multer from 'multer';

const storage = multer.diskStorage({
  destination: function(req, file, callback) {
    callback(null, 'public/uploads/images/');
  },
  filename: function(req, file, callback) {
    callback(null, Date.now() + file.originalname)
  }
});
  
const upload = multer({
  storage: storage,
  limits: {
    fieldSize: 1024 * 1024 * 3
  }
});

const router = express.Router();

router.get('/', getProjects);
router.get('/:id', getProjectDetails);
router.post('/', upload.single('image'), createProject);
router.patch('/:id', updateProject);
router.delete('/:id', deleteProject);
export default router;
