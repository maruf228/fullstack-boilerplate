import express from 'express';
import { createUser, getUsers, updateUser, deleteUser, getUserDetails } from '../controllers/user.js';
// import { auth } from '../middleware/auth.js';

const router = express.Router();

// to create new unique user
router.post('/', createUser);
router.get('/', getUsers);
router.get('/:id', getUserDetails);
router.patch('/:id', updateUser);
router.delete('/:id', deleteUser);

export default router;
