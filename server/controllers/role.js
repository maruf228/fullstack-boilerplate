import mongoose from 'mongoose';
import Role from '../models/role.js';

export const getRoles = async (req, res) => {
    try {
        const roles = await Role.find();
        res.status(200).json(roles);
    } catch (error) {
        res.status(404).json({ message: error });
    }
}

export const getRoleDetails = async (req, res) => {
  try {
    const role = await Role.findById(req.params.id);
    res.status(200).json(role);
  } catch (error) {
    res.status(404).json({ message: error });
  }
}

export const createRole = async (req, res) => {
    const role = req.body;
    const newRole = new Role(role);

    try {
        await newRole.save();
        res.status(201).json(newRole);
    } catch (error) {
        res.status(409).json({ message: error })
    }
}

export const updateRole = async (req, res) => {
    const { id: _id } = req.params;
    const role = req.body;
    if (!mongoose.Types.ObjectId.isValid(_id)) return res.status(404).send('No role with that id');
    const updateRole = await Role.findByIdAndUpdate(_id, { ...role, _id }, { new: true });
    res.json(updateRole);
}

export const deleteRole = async (req, res) => {
    const { id } = req.params;

    if (!mongoose.Types.ObjectId.isValid(id)) return res.status(404).send('No role with that id');
    await Role.findByIdAndDelete(id);
    res.json({ message: 'Role successfully deleted' });
}
