import User from '../models/user.js';
import mongoose from 'mongoose';
import bcrypt from 'bcryptjs';
import config from 'config';
import jwt from 'jsonwebtoken';

export const createUser = async (req, res) => {
  const data = req.body;

  if (!data.name || !data.email || !data.password) {
    return res.status(400).json({ message: 'plz enter all fields'})
  }

  let checkUser = await User.findOne({ email: data.email })

  if(checkUser) return res.status(400).json({ message: 'User already exists'});

  const user = new User(data);

  bcrypt.genSalt(10, (err, salt) => {
    bcrypt.hash(user.password, salt, async (err, hash) => {
      if (err) return err;
      user.password = hash;

      try {
        await user.save();
        jwt.sign({ id: user.id }, config.get('jwtSecret'), { expiresIn: 3600 },
          (err, token) => {
            if (err) return err;
            res.status(201).json({ token, user });
          }
        )
      } catch (error) {
        res.status(409).json({ message: error })
      }
    })
  })
}

export const getUsers = async (req, res) => {
  try {
    const users = await User.find().populate('role');
    res.status(200).json(users);
  } catch (error) {
    res.status(404).json({ message: error });
  }
}

export const getUserDetails = async (req, res) => {
  try {
    const user = await User.findById(req.params.id);
    res.status(200).json(user);
  } catch (error) {
    res.status(404).json({ message: error });
  }
}

export const updateUser = async (req, res) => {
  const { id: _id } = req.params;
  const user = req.body;
  if (!mongoose.Types.ObjectId.isValid(_id)) return res.status(404).send('No usre with that id');
  const updateUser = await User.findByIdAndUpdate(_id, { ...user, _id }, { new: true });
  res.json(updateUser);
}

export const deleteUser = async (req, res) => {
  const { id } = req.params;

  if (!mongoose.Types.ObjectId.isValid(id)) return res.status(404).send('No user with that id');
  await User.findByIdAndDelete(id);
  res.json({ message: 'User successfully deleted' });
}
