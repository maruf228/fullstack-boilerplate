import mongoose from 'mongoose';
import Page from '../models/page.js';

export const getPages = async (req, res) => {
    try {
        const pages = await Page.find();
        res.status(200).json(pages);
    } catch (error) {
        res.status(404).json({ message: error });
    }
}

export const getPageDetails = async (req, res) => {
  try {
    const page = await Page.findById(req.params.id);
    res.status(200).json(page);
  } catch (error) {
    res.status(404).json({ message: error });
  }
}

export const createPage = async (req, res) => {
    const page = req.body;
    const newPage = new Page(page);

    try {
        await newPage.save();
        res.status(201).json(newPage);
    } catch (error) {
        res.status(409).json({ message: error })
    }
}

export const updatePage = async (req, res) => {
    const { id: _id } = req.params;
    const page = req.body;
    if (!mongoose.Types.ObjectId.isValid(_id)) return res.status(404).send('No page with that id');
    const updatePage = await Page.findByIdAndUpdate(_id, { ...page, _id }, { new: true });
    res.json(updatePage);
}

export const deletePage = async (req, res) => {
    const { id } = req.params;

    if (!mongoose.Types.ObjectId.isValid(id)) return res.status(404).send('No page with that id');
    await Page.findByIdAndDelete(id);
    res.json({ message: 'Page successfully deleted' });
}
