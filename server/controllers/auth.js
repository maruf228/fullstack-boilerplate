import User from '../models/user.js';
import bcrypt from 'bcryptjs';
import config from 'config';
import jwt from 'jsonwebtoken';

export const loginUser = async (req, res) => {
  const user = req.body;

  if (!user.email || !user.password) {
    return res.status(400).json({ message: 'plz enter all fields'})
  }

  let getUser = await User.findOne({ email: user.email })

  if(!getUser) {
    return res.status(400).json({ message: 'User does not exists'});
  } else {
    let isMatch = await bcrypt.compare(user.password, getUser.password);

    if (!isMatch) return res.status(400).json({ message: 'Invalid credential'});

    jwt.sign({ id: getUser.id }, config.get('jwtSecret'), { expiresIn: 3600 },
      (err, token) => {
        if(err) return err;
        res.status(201).json({ token, user: getUser });
      }
    )
  }
}

export const getUser = async (req, res) => {
  let user = await User.findById(req.user.id).select('-password');
  res.status(200).json(user);
}
